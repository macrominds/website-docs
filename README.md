# macrominds website-docs

Documentation project for [macrominds/website](https://gitlab.com/macrominds/website) &
[macrominds/website-lib](https://gitlab.com/macrominds/website-lib).
 
~~See the documentation pages hosted on [website.uber.space](https://website.uber.space).~~
The website will be taken down.
Nobody uses this code except for me, so I decided to save the effort to maintain a demo.

## Testing locally

### Requirements

php 8.0 

### Browse the site

```bash
$ php -S localhost:8080 -t ./public ./router.php
```

Then open http://localhost:8080/ in your browser.

### Unit Tests

```bash
$ vendor/bin/phpunit
```

## Deploy

~~[Prepare the deployment](https://website.uber.space/setup/auto-deployment.html).~~
[Prepare the deployment](content/setup/auto-deployment.yml.md).

To deploy, run:

```bash
$ vendor/bin/dep deploy website-docs
```

## Contribution

Please feel free to report errors and to issue Merge Requests with your fixes or improvements.
