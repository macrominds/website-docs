<?php

namespace Deployer;

require 'recipe/common.php';
// require 'recipe/npm.php';

set('allow_anonymous_stats', false);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);

// Hosts
inventory('hosts.yml');

// Tasks
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'set:remote_user',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success',
]);

desc('Set remote user');
task('set:remote_user', function () {
    set('remote_user', run('whoami'));
});

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Uncomment to run "npm install && npm run production" during deployment
//desc('Run npm build process');
//task('npm:build', function () {
//    run('cd {{release_path}} && {{bin/npm}} run production');
//});
//
//desc('Run npm processes');
//task('npm', [
//    'npm:install',
//    'npm:build',
//]);
//
//after('deploy:vendors', 'npm');

// If you are using an Uberspace, see https://gitlab.com/macrominds/provision/uberspace for provisioning helpers
// There's a problem with Uberspace, somehow, after the successful deployment, the change is not noticed by
// php. You need to `uberspace tools restart php`. This is not necessary when you edit a file on the
// server, or when you upload a new version of a file. But using the deployer, it is necessary.
// This might have to do with the deployer checking out in a new directory and then just
// pointing the `current` symlink to the new release. Maybe this change in the symlink
// src is too less to be noticed. It doesn't help to touch any of the symlinks or
// folders. Tried it with html, current, releases, releases/{n},
// releases/{n}/public, current/public and
// current/public/index.php.
//
// Turning off the opcache doesn't help neither.
// Tried that with ~/etc/php.d/disable-opcache.ini:
// opcache.enable = 0
//
// Uncomment this workaround if you are working with an Uberspace:

task('uberspace:reload', function () {
    run('uberspace tools restart php');
});
after('cleanup', 'uberspace:reload');
