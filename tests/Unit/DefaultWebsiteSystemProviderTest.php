<?php

namespace Tests\Unit;

use Macrominds\DefaultWebsiteSystemProvider;
use PHPUnit\Framework\TestCase;

class DefaultWebsiteSystemProviderTest extends TestCase
{
    /**
     * @test
     */
    public function it_provides_at_least_2_links()
    {
        $provider = new DefaultWebsiteSystemProvider();
        $this->assertTrue(count($provider->getLinks()) > 2);
    }

    /**
     * @test
     */
    public function it_provides_all_default_links()
    {
        $provider = new DefaultWebsiteSystemProvider();
        $this->assertEquals(
            array_values(DefaultWebsiteSystemProvider::SYSTEMS),
            $provider->getLinks()
        );
    }
}
