<?php

namespace Tests\Unit;

use Macrominds\WebsiteSystemProvider;
use PHPUnit\Framework\TestCase;

class WebsiteSystemProviderTest extends TestCase
{
    private WebsiteSystemProvider $provider;

    const TEST_SYSTEMS = [
        'WordPress' => 'https://wordpress.org/',
        'django CMS' => 'https://www.django-cms.org',
        'JBake' => 'https://jbake.org/',
    ];

    /**
     * @before
     */
    public function setupWebsiteSystemProvider()
    {
        $this->provider = new WebsiteSystemProvider(self::TEST_SYSTEMS);
    }

    /**
     * @test
     */
    public function it_provides_a_list_of_website_system_links()
    {
        $this->assertEquals(
            array_values(self::TEST_SYSTEMS),
            $this->provider->getLinks()
        );
    }

    /**
     * @test
     */
    public function it_provides_a_certain_link()
    {
        $this->assertEquals(
            self::TEST_SYSTEMS['JBake'],
            $this->provider->getLink(2)
        );
    }

    /**
     * @test
     */
    public function it_calculates_the_correct_maximum_value_for_the_random_index()
    {
        $numberOfItems = count(self::TEST_SYSTEMS);
        $maxIndex = $numberOfItems - 1;
        $this->assertEquals(
            $maxIndex,
            $this->provider->getRandomMaximum()
        );
    }

    /**
     * @test
     */
    public function it_creates_a_random_index_within_the_system_boundaries()
    {
        $randomIndex = $this->provider->getRandomIndex();
        $this->assertTrue($randomIndex < count($this->provider->getLinks()));
        $this->assertTrue($randomIndex >= 0);
    }

    /**
     * @test
     */
    public function the_random_index_varies()
    {
        $index = $this->provider->getRandomIndex();
        $maxTries = 100;
        for ($i = 0; $i < $maxTries; ++$i) {
            $newIndex = $this->provider->getRandomIndex();
            if ($newIndex !== $index) {
                $this->assertNotEquals($newIndex, $index);

                return;
            }
        }
        $this->fail('Reached maximum number of tries. The random index seems not to vary.');
    }

    /**
     * @test
     */
    public function it_provides_a_random_link()
    {
        $this->assertContains($this->provider->getRandomLink(), self::TEST_SYSTEMS);
    }
}
