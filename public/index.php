<?php

use Macrominds\App;
use Macrominds\DefaultConfig;
use Macrominds\DefaultWebsiteSystemProvider;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\MarkdownServiceProvider;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;

require_once __DIR__.'/../vendor/autoload.php'; // @codeCoverageIgnore

$app = new App(realpath(__DIR__.'/..'));

$configuration = (new DefaultConfig())->merge([
    'variables.var.locale' => 'en',
    'template-engine.exported.functions' => [
        'css' => function (string $name): string {
            return "/css/{$name}";
        },
        'cta' => function (string $link, string $text, ...$class): string {
            $classValue = implode(' ', ['cta', ...$class]);

            return "<a class=\"{$classValue}\" href=\"{$link}\">{$text}</a>";
        },
        'link' => function (string $link): string {
            if ('random_website_system' === $link) {
                return (new DefaultWebsiteSystemProvider())->getRandomLink();
            }

            return $link;
        },
    ],
]);

$app->configure($configuration);
$app->registerServiceProviders([
    new FrontmatterServiceProvider($configuration),
    new YamlParserServiceProvider($configuration),
    new MarkdownServiceProvider($configuration),
    new TwigServiceProvider($configuration),
]);
$app->run();
