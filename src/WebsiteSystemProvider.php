<?php

namespace Macrominds;

class WebsiteSystemProvider
{
    private array $systems;

    public function __construct(array $systems)
    {
        $this->systems = $systems;
    }

    public function getLinks(): array
    {
        return array_values($this->systems);
    }

    public function getLink(int $int)
    {
        return $this->getLinks()[$int];
    }

    public function getRandomIndex(): int
    {
        return rand(0, $this->getRandomMaximum());
    }

    public function getRandomMaximum(): int
    {
        return count($this->getLinks()) - 1;
    }

    public function getRandomLink()
    {
        return $this->getLink($this->getRandomIndex());
    }
}
