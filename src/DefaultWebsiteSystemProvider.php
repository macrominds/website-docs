<?php

namespace Macrominds;

class DefaultWebsiteSystemProvider extends WebsiteSystemProvider
{
    public const SYSTEMS = [
        'WordPress' => 'https://wordpress.org/',
        'Drupal™' => 'https://www.drupal.org/',
        'TYPO3 CMS' => 'https://typo3.org/',
        'Contao' => 'https://contao.org/',
        'Grav' => 'https://getgrav.org/',
        'Joomla' => 'https://www.joomla.org/',
        'October CMS' => 'https://octobercms.com/',
        'django CMS' => 'https://www.django-cms.org',
        'Hugo' => 'https://gohugo.io/',
        'JBake' => 'https://jbake.org/',
        'Jigsaw' => 'https://jigsaw.tighten.co/',
        'Pulse CMS' => 'https://www.pulsecms.com/',
        'Statamic' => 'https://statamic.com/',
    ];

    public function __construct()
    {
        parent::__construct(self::SYSTEMS);
    }
}
