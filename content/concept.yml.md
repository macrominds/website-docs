---
title: Concept – macrominds website system
description: Folder structure and architecture
---
# Concept

## Folder structure

The following folders have been setup with minimal example content and templates:

`content`
:   [Markdown content with YAML frontmatter](/concept/yaml-and-markdown.html)

`templates`
:   [Twig templates](/concept/twig-templates.html) for your HTML

`public`
:   is your document root. CSS should go there.


<div class="cta-block" markdown="1">
[Proceed with YAML and Markdown](/concept/yaml-and-markdown.html){.cta .default}
[Proceed with Twig templates](/concept/twig-templates.html){.cta}
</div>
