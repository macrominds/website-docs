---
title: Setup auto deployment – macrominds website system
description: How to setup or remove auto deployment
---
# Setup auto deployment

We're using [Deployer](https://deployer.org/) for auto deployment. 
This dependency is already setup in your composer.json. 

## Preparations

Edit `hosts.yml` to match your git `repository` and `application` name. 

The keys `testing` and `prod` are server names. Either configure your ssh servers (~/.ssh/config) to match
the names or rename the keys to your server(s). E.g. replace `prod` with `my-website.com`. Of course,
you will still need to configure ssh for `my-website.com`.

Example `~/.ssh/config` snippet if you keep `testing` and `prod`:

```
Host testing
HostName ciffreo.uberspace.de
User=staging
IdentityFile ~/.ssh/id_staging_ciffreo_uberspace_de

Host prod
HostName scotti.uberspace.de
User=website
IdentityFile ~/.ssh/id_website_scotti_uberspace_de
```

If you are using an Uberspace, uncomment the following part of your `deploy.php` file. 
It is required as a work around for caching problems on the Uberspace.

```php
task('uberspace:reload', function() {
   run('uberspace tools restart php');
});
after('cleanup', 'uberspace:reload');
```

## Run the auto deployment

First, [set up your server](/setup/server.html).

Then run `vendor/bin/dep deploy {server-name}`, and replace `{server-name}` by one of your 
configured servers. 

E.g. run `vendor/bin/dep deploy prod` if you haven't changed the name of it in 
`hosts.yml`.

<div class="cta-block" markdown="1">
[Learn the core concepts](/concept.html){.cta .default}
[Setup manual deployment](/setup/auto-deployment.html){.cta}
</div>
