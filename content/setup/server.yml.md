---
title: Setup the server – macrominds website system
description: How to setup the server
---
# Setup the server

See [automatic provisioning tools](#automatic-provisioning-tools) 
for semi-automatic setup.

Or setup your server manually.

## Manual setup

### Setup the document root                                      {#set-the-document-root}

Make [public](public) your document root.
TIP: If you cannot easily modify your document root, try a symlink 
(e.g. `ln -sfn website/public docroot`).

### Logrotate (optional)

Schedule a cronjob that performs the linux native logrotate. 

See [macrominds/provision/uberspace/web-logrotate](https://gitlab.com/macrominds/provision/uberspace/web-logrotate) for uberspace provisioning.

Basically, you will want to make a cronjob like this:

```
@daily /usr/sbin/logrotate -s /home/my-website/.logrotate.status /home/my-website/etc/web-logrotate.conf
```

And a web-logrotate.conf like this:

```
weekly

/var/www/virtual/my-website/shared/log/*.log {
    weekly
    missingok
    rotate 8
}
```

## Automatic provisioning tools                                  {#automatic-provisioning-tools}

I recommend using Uberspace. 

See https://gitlab.com/macrominds/provision/uberspace for ansible roles
that are specially tailored for Uberspace and macrominds/website.

You can combine them as follows. Configure ssh access for your Uberspace and 
run `ansible-playbook -i my-website, site.yml` to provision it. 
Most likely, I will provide a detailed example project soon.

site.yml:

```yaml
---
- hosts: all
  roles:
    - role: lib-webp
    - role: web-domains
      web_domains_list:
        - my-website.com
        - www.my-website.com
    - role: mail-domains
      mail_domains_list:
        - my-website.com

    - role: web-prerequisites
    - role: web-provide-shared-deployer-dot-env
    - role: web-logrotate

    - role: deployment-key
      deployment_key_local_path: ~/.ssh/my-deploy-key

    - role: web-link-html-to-deployer-target
```

An Uberspace server, provisioned that way, is ready to be used for auto deployment.

<div class="cta-block" markdown="1">
[Setup auto deployment](/setup/auto-deployment.html){.cta .default}
[Manual deployment](/setup/manual-deployment.html){.cta}
</div>
