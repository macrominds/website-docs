---
title: Setup auto deployment – macrominds website system
description: How to setup or remove auto deployment
---
# Manual deployment

You can delete `deploy.php` and `hosts.yml` if you don't plan to use auto deployment.
In that case, you can also run `composer remove deployer/deployer deployer/recipes`.

Don't forget to [set your document root](/setup/server.html#set-the-document-root) after having performed 
the steps of either [Pure manual deployment](#pure-manual-deployment) or 
[Manual deployment with composer](#manual-deployment-with-composer) 
for the first time.

## Pure manual deployment                                        {#pure-manual-deployment}

For pure manual deployment, run `composer install --no-dev --optimize-autoloader` locally.
Afterwards upload the following files and folders:

* content
* templates
* public
* vendor
* .env **But make sure to adapt it to production settings**: `APP_ENV=production`

## Manual deployment with composer                               {#manual-deployment-with-composer}

If you have Composer and ssh access, then upload

* content
* templates
* public
* composer.json
* composer.lock
* .env **But make sure to adapt it to production settings**: `APP_ENV=production`

On the server, run `composer install --no-dev --optimize-autoloader`.

<div class="cta-block" markdown="1">
[Learn the core concepts](/concept.html){.cta .default}
[Setup auto deployment](/setup/auto-deployment.html){.cta}
</div>
