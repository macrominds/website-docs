---
is_template: true
title: macrominds website system – flat file websites
description: Flat file websites based on markdown
actions:
 - text: Proceed with the Setup
   link: /setup.html
   class: default
 - text: Take me to another popular solution
   link: random_website_system
---
# macrominds website system

## Why would I want yet another website system?

You probably don't.

But if you are a php developer who appreciates standards and if 
you nod and mumble „yes“ at some of the following statements, 
then it might be worth getting to know macrominds/website.

* You want to implement websites and web designs without system restrictions
* You don't want to use databases for simple websites
* You want to work offline and locally
* You like to version control the code **and** the content
* You appreciate performance and control
* A static site generator doesn't meet your needs
* You think a CMS should be optional, not tied to the website
* You want sensitive code to be stored outside the document root
* You want tested and testable code and you like automated deployment

## This is the macrominds website system

Just type `composer create-project macrominds/website my-website` and 
start working with the 
[project skeleton](https://gitlab.com/macrominds/website/) 
that provides you with:

- flat file content
- content that can be version controlled 
- [Twig Templates](https://twig.symfony.com/)
- [YAML](https://en.wikipedia.org/wiki/YAML) frontmatter
- [Markdown content](https://en.wikipedia.org/wiki/Markdown)
- and the ability to swap them for other engines
- auto deployment
- resource oriented urls
- sensitive code outside document root
- tested and testable code
 
The [core](https://gitlab.com/macrominds/website-lib/) is a composer
dependency, so that it doesn't clutter up your project.

Oh, and it's [open source](https://gitlab.com/macrominds/website-lib/blob/master/LICENSE).

## Alpha status

There are actually live websites powered by macrominds/website-lib, but the framework 
is in an extreme early stage – please keep that in mind.

<div class="cta-block">
{% for cta in actions %}
  {{ cta(link(cta.link), cta.text, cta.class)|raw }}
{% endfor %}
</div>

