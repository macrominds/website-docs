---
title: YAML and Markdown – macrominds website system
description: How YAML frontmatter and markdown content works with the macrominds website system
---
# YAML and Markdown

The content is managed in the `content` folder. 
It is resource oriented, thus its structure defines the available links.

Out of the box, the macrominds website system comes with Markdown content 
and YAML frontmatter. Our files therefore have the suffix `.yml.md`, 
which helps your tools to support Syntax highlighting.

A file `content/setup.yml.md` will be available at `/setup.html` out of the box.

A file looks like this:

```md
---
title: page title
description: meta description
---
# Heading

Markdown content
```

The first section – delimited by `---` – is the YAML frontmatter.

See [the wikipedia YAML documentation](https://en.wikipedia.org/wiki/YAML) for details.
   
You can use it to define variables for the Twig templates.

```md
---
author: Thomas Praxl
---
Now the variable `author` can be used within your twig template.
```

You can use it to change the Twig template: 

```md
---
template:demo
---
# Now using the demo.html.twig template
```

You can use it to specify that your markdown content contains Twig syntax and shall be evaluated:

```md
---
is_template: true
author: Thomas Praxl
---
Now your markdown is evaluated as a Twig template
and the variable `author` below gets inserted.

Author: {{ author }}.
```

You also have access to the requested path:

```md
is_template: true
---
Requested path: {{ requested_path }}.
```

The second section is the Markdown content. You usually specify your page content here.

See [the wikipedia Markdown documentation](https://en.wikipedia.org/wiki/Markdown) for details.

<div class="cta-block" markdown="1">
[Proceed with Twig templates](/concept/twig-templates.html){.cta .default}
</div>
