---
title: Twig templates – macrominds website system
description: How the Twig templates with within the macrominds website system
---
# Twig templates

See the [official Twig documentation](https://twig.symfony.com/) for details about Twig.

## Exporting functions to twig

In your `public/index.php`:

```php
use Macrominds\App;
use Macrominds\DefaultConfig;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\MarkdownServiceProvider;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new App(realpath(__DIR__ . '/..'));

$configuration = (new DefaultConfig())->merge([
    'template-engine' => [
        'exported'   => [
            'functions' => [
                'css' => function (string $name): string {
                    return "/css/{$name}";
                },
            ],
        ],
    ],
]);
$app->configure($configuration);
$app->registerServiceProviders([
    new FrontmatterServiceProvider($configuration),
    new YamlParserServiceProvider($configuration),
    new MarkdownServiceProvider($configuration),
    new TwigServiceProvider($configuration),
]);
$app->run();
```

Usage in twig template:

```twig
<link href="{{ css("/css/app.css") }}" rel="stylesheet">
```

<div class="cta-block" markdown="1">
[Proceed with YAML and Markdown](/concept/yaml-and-markdown.html){.cta .default}
</div>

