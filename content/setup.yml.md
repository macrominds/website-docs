---
title: Setup and Quickstart – macrominds website system
description: Learn what you need to get started with the macrominds website system 
---
# Setup

## Server system requirements

- php >= 7.3
- mod_rewrite

Composer, git and ssh access are required for auto deployment.

Until [macrominds/website-lib #23](https://gitlab.com/macrominds/website-lib/issues/23)
is fixed, you need [ext-intl](https://www.php.net/manual/en/book.intl.php) as well.  
Phpbrew users install the intl extension with `phpbrew ext install intl`.

## Create a new website

Create a new website with `composer create-project macrominds/website my-website`.
Replace `my-website` with your desired project name.

## Run the demo server 

In the website root:

```bash
php -S localhost:8080 -t ./public/ ./router.php
```

## First steps

### Settings: local vs. production

Adapt the `.env` file to your needs.

The `.env` file has been provided to you with `APP_ENV=production` to prevent the exposure of sensitive information. 
This is the safe setting that is meant to be used on a production server.

In a local development environment, you typically want `APP_ENV=local` to expose exception messages and details.

### Cleanup composer.json after creation

Optionally **remove** the following section from your composer.json.
It's a leftover that doesn't harm, but it clutters up your composer.json a bit.

```json
    "scripts": {
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ]
    },
```

Adapt the project name and description of your composer.json:

```json
     "name": "acme/website",
     "description": "The well known ACME website source"
```

Don't forget to run `composer update foo/bar` to update your composer.lock file 
after you have manually modified composer.json.

<div class="cta-block" markdown="1">
[Setup the server](/setup/server.html){.cta .default}
</div>
